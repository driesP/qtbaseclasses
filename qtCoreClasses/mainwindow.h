#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "filehandler.h"
#include "regexhandler.h"
#include "painterclass.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_write_clicked();

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    fileHandler *fh;
    regexHandler *rh;
    painterClass *ph;
};

#endif // MAINWINDOW_H
