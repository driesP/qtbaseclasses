#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    fh = new fileHandler();
    rh = new regexHandler();
    QStringList temp;
    temp = rh->searchString("(\\d+)","Offsets: 12 14 99 231 7");
    for(int i =0; i < temp.length(); i++)
    {
        __debug << temp.at(i);
    }
    ph = new painterClass();
    ph->show();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_write_clicked()
{

    fh->writeFile(ui->write_text->toPlainText());
    fh->closeFile();
}

void MainWindow::on_pushButton_clicked()
{
    QString returnData = fh->readFile();
    ui->read_text->appendPlainText(returnData);
    fh->closeFile();
}
