#include "filehandler.h"

fileHandler::fileHandler(QObject *parent) : QObject(parent)
{
    __debug << "File Object Created";
}
fileHandler::~fileHandler()
{
    __debug << "File Object Destructed";
}
void fileHandler::writeFile(QString data)
{
    QString FileName = QFileDialog::getOpenFileName();
    file = new QFile(FileName);
    if(!file->open(QIODevice::Append | QIODevice::Text))
        return;
    QTextStream out(file);
    out << data;
}
void fileHandler::writeFile(QString data, QString fileName)
{
    file = new QFile(fileName);
    if(!file->open(QIODevice::Append | QIODevice::Text))
        return;
    QTextStream out(file);
    out << data;
}
QString fileHandler::readFile()
{
    QString FileName = QFileDialog::getOpenFileName();
    file = new QFile(FileName);
    if(!file->open(QIODevice::ReadOnly | QIODevice::Text))
        return "Error Reading File";
    QTextStream in(file);

    return in.readAll();
}
QString fileHandler::readFile(QString fileName)
{
    file = new QFile(fileName);
    if(!file->open(QIODevice::ReadOnly | QIODevice::Text))
        return "Error Reading File";
    QTextStream in(file);

    return in.readAll();
}


void fileHandler::closeFile()
{
    file->close();
}
