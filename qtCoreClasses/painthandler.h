#ifndef PAINTHANDLER_H
#define PAINTHANDLER_H

#include <QObject>
#include <QWidget>
#include <QDebug>
#include <QPainter>

#define __debug qDebug() << __FILE__ << __LINE__

class paintHandler : public QObject
{
    Q_OBJECT
public:
    explicit paintHandler(QObject *parent = 0);
    ~paintHandler();
signals:

public slots:
private:
    QWidget *wg;
};

#endif // PAINTHANDLER_H
