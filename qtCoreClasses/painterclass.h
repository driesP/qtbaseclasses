#ifndef PAINTERCLASS_H
#define PAINTERCLASS_H

#include <QWidget>
#include <QDebug>
#include <QPainter>

#define __debug qDebug() << __FILE__ << __LINE__


class painterClass : public QWidget
{
    Q_OBJECT
public:
    explicit painterClass(QWidget *parent = 0);
    ~painterClass();
    void paintEvent(QPaintEvent *);
signals:

public slots:


};

#endif // PAINTERCLASS_H
