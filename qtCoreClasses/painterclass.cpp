#include "painterclass.h"

painterClass::painterClass(QWidget *parent) : QWidget(parent)
{
    __debug << "painterClass constructed";
    this->setWindowTitle("painter window");
}

painterClass::~painterClass()
{
    __debug << "painterClass destructed";
}

void painterClass::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    QPen pen(Qt::red,3);
    p.setPen(pen);
    p.drawRect(10,10,150,150);
}
