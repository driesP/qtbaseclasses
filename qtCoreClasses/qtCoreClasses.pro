#-------------------------------------------------
#
# Project created by QtCreator 2016-05-25T08:43:02
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qtCoreClasses
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    filehandler.cpp \
    regexhandler.cpp \
    painterclass.cpp

HEADERS  += mainwindow.h \
    filehandler.h \
    regexhandler.h \
    painterclass.h

FORMS    += mainwindow.ui

DISTFILES += \
    regexCheat
