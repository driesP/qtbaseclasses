#include "regexhandler.h"

regexHandler::regexHandler(QObject *parent) : QObject(parent)
{
    __debug << "regexHandler Created";
}

regexHandler::~regexHandler()
{
    __debug << "regexHandler Destroyed";
}
QStringList regexHandler::searchString(QString exp, QString searchString)
{
    rx = new QRegExp(exp);
    QStringList tempList;
    int pos = 0;

    while((pos = rx->indexIn(searchString,pos))!= -1)
    {
        tempList << rx->cap(1);
        pos += rx->matchedLength();
    }
    return tempList;

}
