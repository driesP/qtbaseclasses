#ifndef REGEXHANDLER_H
#define REGEXHANDLER_H

#include <QObject>
#include <QRegExp>
#include <QDebug>
#define __debug qDebug() << __FILE__ << __LINE__

class regexHandler : public QObject
{
    Q_OBJECT
public:
    explicit regexHandler(QObject *parent = 0);
    ~regexHandler();
    QStringList searchString(QString exp, QString searchString);

signals:

public slots:
private:
    QRegExp *rx;
};

#endif // REGEXHANDLER_H
