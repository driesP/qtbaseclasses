#ifndef FILEHANDLER_H
#define FILEHANDLER_H

#include <QObject>
#include <QFile>
#include <QDebug>
#include <QFileDialog>

#define __debug qDebug() << __FILE__ << __LINE__

class fileHandler : public QObject
{
    Q_OBJECT
public:
    explicit fileHandler(QObject *parent = 0);
    ~fileHandler();
    void writeFile(QString data);
    void writeFile(QString data, QString fileName);
    QString readFile();
    QString readFile(QString fileName);
    void closeFile();

signals:

public slots:

private:
    QFile *file;
};

#endif // FILEHANDLER_H
